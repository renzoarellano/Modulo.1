-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: icinema
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `peliculas`
--

DROP TABLE IF EXISTS `peliculas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `peliculas` (
  `idPeliculas` int(11) NOT NULL,
  `Nombre` varchar(45) NOT NULL,
  `Horario` varchar(45) NOT NULL,
  `Imagen` char(75) DEFAULT NULL,
  `Descripcion` text,
  PRIMARY KEY (`idPeliculas`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `peliculas`
--

LOCK TABLES `peliculas` WRITE;
/*!40000 ALTER TABLE `peliculas` DISABLE KEYS */;
INSERT INTO `peliculas` VALUES (1,'Harry Potter y la piedra Filosofal','17:00','C:\\Users\\hecto\\Documents\\Proyectos\\iCinema1.0\\Imagenes\\1.jpg','Harry Potter es un niño huérfano que vive con sus únicos parientes vivos, la familia Dursley, en un suburbio inglés. En su cumpleaños número 11, Harry es visitado por un misterioso individuo llamado Rubeus Hagrid, quien le revela que realmente él es un mago bastante popular en el mundo mágico por haber sobrevivido al ataque mortal de Lord Voldemort cuando sólo tenía un año de edad. Tras haber asesinado a sus padres (James y Lily Potter), Voldemort intentó atacar a Harry pero no consiguió matarlo, dejándole solamente una cicatriz en forma de rayo sobre su frente. Sabiendo esto, Hagrid acompaña a Harry para comprar lo necesario con tal de comenzar su formación en el Colegio Hogwarts de Magia y Hechicería.\n\nSin el consentimiento de sus tíos, Harry Potter asiste a Hogwarts para empezar a aprender conjuros y conocer a nuevos amigos; en el proceso, también inicia sus rivalidades con otros estudiantes y parte del personal del colegio. Durante su estancia, Harry Potter descubre que la institución alberga un misterioso objeto en sus dominios el cual es conocido como la piedra filosofal. Este poderoso elemento es buscado discretamente por Voldemort, a quien la comunidad mágica daba por muerto tras haberle rebotado el ataque mortal dirigido contra Harry Potter. Contrario a dichas creencias, su espíritu desea afanosamente encontrar la piedra para recuperar sus antiguas habilidades de brujo. Además, la piedra filosofal es conocida por otorgar igualmente la inmortalidad a su poseedor.'),(2,'Harry Potter y la camara secreta','18:00','C:\\Users\\hecto\\Documents\\Proyectos\\iCinema1.0\\Imagenes\\2.jpg','Tras derrotar una vez más a lord Voldemort, su siniestro enemigo en Harry Potter y la piedra filosofal, Harry espera impaciente en casa de sus insoportables tíos el inicio del segundo curso del Colegio Hogwarts de Magia. Sin embargo, la espera dura poco, pues un elfo aparece en su habitación y le advierte que una amenaza mortal se cierne sobre la escuela. Así pues, Harry no se lo piensa dos veces y, acompañado de Ron, su mejor amigo, se dirige a Hogwarts en un coche volador. Pero ¿puede un aprendiz de mago defender la escuela de los malvados que pretenden destruirla? Sin saber que alguien había abierto la Cámara de los Secretos, dejando escapar una serie de monstruos peligrosos, Harry y sus amigos Ron y Hermione tendrán que enfrentarse con arañas gigantes, serpientes encantadas, fantasmas enfurecidos y, sobre todo, con la mismísima reencarnación de su más temible adversario.'),(3,'Harry Potter y el prisionero de Azcaban','15:00','C:\\Users\\hecto\\Documents\\Proyectos\\iCinema1.0\\Imagenes\\3.jpg','Igual que en las dos primeras partes de la serie, Harry aguarda con impaciencia el inicio del tercer curso en el Colegio Hogwarts de Magia. Tras haber cumplido los trece años, solo y lejos de sus amigos, Harry se pelea con su bigotuda tía Marge, a la que convierte en globo, y debe huir en un autobús mágico. Mientras tanto, de la prisión de Azkaban se ha escapado un terrible villano, Sirius Black, un asesino en serie con poderes mágicos que fue cómplice de lord Voldemort y que parece dispuesto a borrar a Harry del mapa. Y por si fuera poco, Harry deberá enfrentarse también a unos terribles monstruos, los dementores, seres abominables capaces de robarle la felicidad a los magos y de eliminar todo recuerdo hermoso de aquellos que osan mirarlos. Lo que ninguno de estos malvados personajes sabe es que Harry, con la ayuda de sus fieles amigos Ron y Hermione, es capaz de todo y mucho más.'),(4,'Harry Potter y el calix de Fuego','20:00','C:\\Users\\hecto\\Documents\\Proyectos\\iCinema1.0\\Imagenes\\4.jpg','A lo largo de las tres novelas anteriores pertenecientes a la saga de Harry Potter, el protagonista, Harry Potter lucha con las dificultades acarreadas por su adolescencia y por el hecho de ser un mago famoso. Cuando Harry era un niño pequeño, Voldemort, el mago tenebroso más poderoso de la historia, había asesinado a los padres de Harry, pero luego se había desvanecido misteriosamente despuès de que su maldición asesina en contra de Harry rebotase. El hecho había causado que Harry adquiriera fama inmediata y que fuese colocado bajo los cuidados de sus tíos muggles, Petunia y Vernon, quienes a su vez tenían un hijo llamado Dudley Dursley.\nHarry descubre que es un mago a los once años de edad e ingresa en el Colegio Hogwarts de Magia y Hechicería. Allí, crea amistad con Ron Weasley y Hermione Granger y se enfrenta con Lord Voldemort, quien en varias ocasiones intenta regresar al poder. En el primer año de Harry en la escuela, debe proteger la Piedra Filosofal para evitar que Voldemort y uno de sus fieles vasallos, un profesor de Hogwarts, la obtuviesen. Después de regresar a la escuela, pasadas las vacaciones de verano, los estudiantes de Hogwarts sufren ataques debido a la apertura de la legendaria \"Cámara de los Secretos\". Harry termina con los ataques cuando mata a un basilisco) y frustra un nuevo intento de Lord Voldemort para recuperar su fortaleza. Al año siguiente, Harry descubre que era el principal objetivo de un asesino prófugo llamado Sirius Black. Pese a las extremas medidas de seguridad que se aplican en Hogwarts, Harry se encuentra con Black al final de su tercer curso, y descubre que en realidad éste es inocente y es su padrino. También descubre que Peter Pettigrew, el amigo de Sirius y James Potter, había sido el que había traicionado a sus padres y entregado a Voldemort.'),(5,'Harry Potter y la orden del Fenix','19:00','C:\\Users\\hecto\\Documents\\Proyectos\\iCinema1.0\\Imagenes\\5.jpg','La orden del fénix, el ejército que creó Albus Dumbledore (Michael Gambon) para derrotar al que no debe ser nombrado hace 15 años, ha resurgido para enfrentarse a los mortífagos una vez más. Aunque el Ministerio de Magia no lo acepte, Lord Voldemort (Ralph Fiennes) ha regresado para someter al mundo mágico bajo su influencia. Y para ello, primero tiene que asesinar a Harry Potter (Daniel Radcliffe). El Ministro, como medida para mantener a raya a Dumbledore y a la Orden del Fénix, ha decidido introducir a la profesora Umbridge (Imelda Staunton) en Hogwarts y ésta impondrá un régimen dictatorial que enfrentará a varios alumnos.'),(6,'Harry Potter y el principe Mestizo','23:00','C:\\Users\\hecto\\Documents\\Proyectos\\iCinema1.0\\Imagenes\\6.jpg','Lord Voldemort va tomando el control tanto del mundo Muggle como del mundo mágico, y Hogwarts ya no es el lugar seguro que solía ser. Harry sospecha incluso que el peligro está dentro del castillo. Dumbledore sabe que la batalla final se aproxima, y decide preparar a Harry. Con ese fin, Dumbledore busca la ayuda de su viejo amigo y colega, el profesor Horace Slughorn, que cree que tiene información muy importante. Entre tanto, entre los muros de la escuela, los estudiantes se encuentran afectados por algo que siempre ataca a los adolescentes: las hormonas. Harry cada vez se siente más atraído por Ginny, pero eso también le pasa a Dean Thomas. Por su parte, Lavender Brown ha decidido que Ron tiene que ser para ella, pero que no contó con los chocolates de Romilda Vane, y también está Hermione, ardiendo de celos e incluso llorando por Ron, pero decidida a no mostrar sus sentimientos. Los romances nacen, un estudiante y un profesor actúan de manera extraña, al parecer nada a favor del bien, y una tragedia dejará con un gran vacío a Hogwarts, un lugar que tal vez ya nunca será como antes.');
/*!40000 ALTER TABLE `peliculas` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-09  1:31:33
